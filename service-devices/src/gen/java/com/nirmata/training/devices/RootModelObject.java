/**
 * Generated ModelObject: Root
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import java.util.*;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class RootModelObject extends AbstractModelObject {
    private String _modelId;
    private String _modelName;

	private List <ModelId> _devices;
	private List <ModelId> _logs;
	private List <ModelId> _alarmTypes;
	private List <ModelId> _alarmNotifiers;

    RootModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    	_devices = new ArrayList<>();
    	_logs = new ArrayList<>();
    	_alarmTypes = new ArrayList<>();
    	_alarmNotifiers = new ArrayList<>();
    }

    private RootModelObject(RootModelObject obj) {
    	super(obj);

    	_modelId = obj._modelId;
    	_modelName = obj._modelName;
    	_devices = new ArrayList<>(obj._devices);	
    	_logs = new ArrayList<>(obj._logs);	
    	_alarmTypes = new ArrayList<>(obj._alarmTypes);	
    	_alarmNotifiers = new ArrayList<>(obj._alarmNotifiers);	
    }

    @Override
    public RootModelObject copy() {
    	return new RootModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    Root fields = (Root) fieldIndex;
		switch (fields)
	    {
			case modelId:
			    return _modelId;

			case modelName:
			    return _modelName;

			case devices:
			    return Collections.unmodifiableList(_devices);

			case logs:
			    return Collections.unmodifiableList(_logs);

			case alarmTypes:
			    return Collections.unmodifiableList(_alarmTypes);

			case alarmNotifiers:
			    return Collections.unmodifiableList(_alarmNotifiers);
					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    Root fields = (Root) fieldIndex;
	    switch (fields) {
	        case modelId:
	            _modelId = (String) data;
	            break;
	        case modelName:
	            _modelName = (String) data;
	            break;
	        case devices:
	            _devices = new ArrayList<>((List<ModelId>) data);
	        	break;

	        case logs:
	            _logs = new ArrayList<>((List<ModelId>) data);
	        	break;

	        case alarmTypes:
	            _alarmTypes = new ArrayList<>((List<ModelId>) data);
	        	break;

	        case alarmNotifiers:
	            _alarmNotifiers = new ArrayList<>((List<ModelId>) data);
	        	break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    Root fields = (Root) fieldIndex;
	    switch (fields) {
		case devices:
		    _devices.add(relationId);
		    break;
		case logs:
		    _logs.add(relationId);
		    break;
		case alarmTypes:
		    _alarmTypes.add(relationId);
		    break;
		case alarmNotifiers:
		    _alarmNotifiers.add(relationId);
		    break;				
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    Root fields = (Root) fieldIndex;
		switch (fields) {
			case devices: {
			    _devices.remove(relationId);
			    break;
			}

			case logs: {
			    _logs.remove(relationId);
			    break;
			}

			case alarmTypes: {
			    _alarmTypes.remove(relationId);
			    break;
			}

			case alarmNotifiers: {
			    _alarmNotifiers.remove(relationId);
			    break;
			}

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("Root")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("modelId", _modelId)
	        .add("modelName", _modelName)
			.add("devices", _devices)
			.add("logs", _logs)
			.add("alarmTypes", _alarmTypes)
			.add("alarmNotifiers", _alarmNotifiers)				        
	        .toString();
	}

}

