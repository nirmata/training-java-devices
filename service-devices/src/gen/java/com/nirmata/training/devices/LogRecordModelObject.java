/**
 * Generated ModelObject: LogRecord
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class LogRecordModelObject extends AbstractModelObject {
    private String _message;
    private Long _timestamp;

    LogRecordModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    }

    private LogRecordModelObject(LogRecordModelObject obj) {
    	super(obj);

    	_message = obj._message;
    	_timestamp = obj._timestamp;
    }

    @Override
    public LogRecordModelObject copy() {
    	return new LogRecordModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    LogRecord fields = (LogRecord) fieldIndex;
		switch (fields)
	    {
			case message:
			    return _message;

			case timestamp:
			    return _timestamp;

					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    LogRecord fields = (LogRecord) fieldIndex;
	    switch (fields) {
	        case message:
	            _message = (String) data;
	            break;
	        case timestamp:
	            _timestamp = (Long) data;
	            break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    LogRecord fields = (LogRecord) fieldIndex;
	    switch (fields) {
					
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    LogRecord fields = (LogRecord) fieldIndex;
		switch (fields) {

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("LogRecord")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("message", _message)
	        .add("timestamp", _timestamp)
					        
	        .toString();
	}

}

