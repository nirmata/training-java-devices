/**
 * Generated ModelClass: PortStatus
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class PortStatusModelClass extends AbstractModelClass {
    PortStatusModelClass(String serviceName) {
        super(serviceName, Devices.PortStatus);

    	setApiLabel("portStatuses");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Port);
    }

	protected void initializeAttributes() {
	    EnumField administrativeStateField = new EnumField(PortStatus.administrativeState, PortAdministrativeState.modelEnums());
	    administrativeStateField.setRequired(true);
	    administrativeStateField.setLabel(false);
	    administrativeStateField.setUnique(UniqueScope.NONE);
	    administrativeStateField.setSystemAccess(FieldAccess.readWrite);
	    administrativeStateField.setUserAccess(FieldAccess.readWrite);
	    setAttribute(PortStatus.administrativeState, administrativeStateField);

	    EnumField operationalStateField = new EnumField(PortStatus.operationalState, PortOperationalState.modelEnums());
	    operationalStateField.setRequired(true);
	    operationalStateField.setLabel(false);
	    operationalStateField.setUnique(UniqueScope.NONE);
	    operationalStateField.setSystemAccess(FieldAccess.readWrite);
	    operationalStateField.setUserAccess(FieldAccess.readCreate);
	    setAttribute(PortStatus.operationalState, operationalStateField);

	    StringField diagnosticField = new StringField(PortStatus.diagnostic);
	    diagnosticField.setRequired(true);
	    diagnosticField.setLabel(false);
	    diagnosticField.setUnique(UniqueScope.NONE);
	    diagnosticField.setSystemAccess(FieldAccess.readWrite);
	    diagnosticField.setUserAccess(FieldAccess.readCreate);
	    diagnosticField.setConstraint("length", "128");
	    diagnosticField.setConstraint("default", "none");
	    setAttribute(PortStatus.diagnostic, diagnosticField);
	}

	protected void initializeRelations() {
	}

	@Override
	protected PortStatusModelObject createInstance(ModelId id, ModelId parentId) {
	    return new PortStatusModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}