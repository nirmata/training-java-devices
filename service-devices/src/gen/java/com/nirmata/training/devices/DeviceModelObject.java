/**
 * Generated ModelObject: Device
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import java.util.*;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class DeviceModelObject extends AbstractModelObject {
    private String _serialNumber;
    private DeviceModel _model;
    private String _firmwareVersion;
    private Long _sysUpTime;

	private ModelId _config;
	private ModelId _status;
	private List <ModelId> _ports;

    DeviceModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    	_config = null;
    	_status = null;
    	_ports = new ArrayList<>();
    }

    private DeviceModelObject(DeviceModelObject obj) {
    	super(obj);

    	_serialNumber = obj._serialNumber;
    	_model = obj._model;
    	_firmwareVersion = obj._firmwareVersion;
    	_sysUpTime = obj._sysUpTime;
    	_config = obj._config;	    
    	_status = obj._status;	    
    	_ports = new ArrayList<>(obj._ports);	
    }

    @Override
    public DeviceModelObject copy() {
    	return new DeviceModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    Device fields = (Device) fieldIndex;
		switch (fields)
	    {
			case serialNumber:
			    return _serialNumber;

			case model:
			    return _model;

			case firmwareVersion:
			    return _firmwareVersion;

			case sysUpTime:
			    return _sysUpTime;

			case config:
			    return _config;

			case status:
			    return _status;

			case ports:
			    return Collections.unmodifiableList(_ports);
					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    Device fields = (Device) fieldIndex;
	    switch (fields) {
	        case serialNumber:
	            _serialNumber = (String) data;
	            break;
	        case model:
	            _model = (DeviceModel) data;
	            break;
	        case firmwareVersion:
	            _firmwareVersion = (String) data;
	            break;
	        case sysUpTime:
	            _sysUpTime = (Long) data;
	            break;
	        case config:
	            _config = (ModelId) data;
	        	break;

	        case status:
	            _status = (ModelId) data;
	        	break;

	        case ports:
	            _ports = new ArrayList<>((List<ModelId>) data);
	        	break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    Device fields = (Device) fieldIndex;
	    switch (fields) {
		case config:
		    _config = relationId;
		    break;
		case status:
		    _status = relationId;
		    break;
		case ports:
		    _ports.add(relationId);
		    break;				
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    Device fields = (Device) fieldIndex;
		switch (fields) {
			case config: {
			    if (_config != null && _config.equals(relationId)) {
			        _config = null;
			    }
			    break;
			}

			case status: {
			    if (_status != null && _status.equals(relationId)) {
			        _status = null;
			    }
			    break;
			}

			case ports: {
			    _ports.remove(relationId);
			    break;
			}

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("Device")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("serialNumber", _serialNumber)
	        .add("model", _model)
	        .add("firmwareVersion", _firmwareVersion)
	        .add("sysUpTime", _sysUpTime)
			.add("config", _config)
			.add("status", _status)
			.add("ports", _ports)				        
	        .toString();
	}

}

