/**
 * Generated ModelClassEnum: Port
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum Port implements FieldIndex {
    macAddress,
    type,
    portNumber,
    status,
    stats;

    @Override
    public String getName() {
        return name();
    }  
}