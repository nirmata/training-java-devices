/**
 * Generated ModelClassEnum: LogRecord
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum LogRecord implements FieldIndex {
    message,
    timestamp;

    @Override
    public String getName() {
        return name();
    }  
}