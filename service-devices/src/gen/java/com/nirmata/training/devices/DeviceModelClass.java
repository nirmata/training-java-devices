/**
 * Generated ModelClass: Device
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class DeviceModelClass extends AbstractModelClass {
    DeviceModelClass(String serviceName) {
        super(serviceName, Devices.Device);

    	setApiLabel("devices");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Root);
    }

	protected void initializeAttributes() {
	    StringField serialNumberField = new StringField(Device.serialNumber);
	    serialNumberField.setRequired(true);
	    serialNumberField.setLabel(true);
	    serialNumberField.setUnique(UniqueScope.PARENT);
	    serialNumberField.setSystemAccess(FieldAccess.readWrite);
	    serialNumberField.setUserAccess(FieldAccess.readWrite);
	    serialNumberField.setConstraint("length", "30");
	    setAttribute(Device.serialNumber, serialNumberField);

	    EnumField modelField = new EnumField(Device.model, DeviceModel.modelEnums());
	    modelField.setRequired(true);
	    modelField.setLabel(false);
	    modelField.setUnique(UniqueScope.NONE);
	    modelField.setSystemAccess(FieldAccess.readWrite);
	    modelField.setUserAccess(FieldAccess.readCreate);
	    setAttribute(Device.model, modelField);

	    StringField firmwareVersionField = new StringField(Device.firmwareVersion);
	    firmwareVersionField.setRequired(true);
	    firmwareVersionField.setLabel(false);
	    firmwareVersionField.setUnique(UniqueScope.NONE);
	    firmwareVersionField.setSystemAccess(FieldAccess.readWrite);
	    firmwareVersionField.setUserAccess(FieldAccess.readWrite);
	    firmwareVersionField.setConstraint("length", "80");
	    setAttribute(Device.firmwareVersion, firmwareVersionField);

	    LongField sysUpTimeField = new LongField(Device.sysUpTime);
	    sysUpTimeField.setRequired(true);
	    sysUpTimeField.setLabel(false);
	    sysUpTimeField.setUnique(UniqueScope.NONE);
	    sysUpTimeField.setSystemAccess(FieldAccess.readWrite);
	    sysUpTimeField.setUserAccess(FieldAccess.readWrite);
	    sysUpTimeField.setConstraint("default", "0");
	    sysUpTimeField.setConstraint("min", "0");
	    setAttribute(Device.sysUpTime, sysUpTimeField);
	}

	protected void initializeRelations() {
	    ChildField configField = new ChildField(Device.config,
	    	Devices.DeviceConfig,
	    	Cardinality.one);
	    setRelation(Device.config, configField);
	    configField.setSystemAccess(FieldAccess.readWrite);
	    configField.setUserAccess(FieldAccess.readWrite);

	    ChildField statusField = new ChildField(Device.status,
	    	Devices.DeviceStatus,
	    	Cardinality.one);
	    setRelation(Device.status, statusField);
	    statusField.setSystemAccess(FieldAccess.readWrite);
	    statusField.setUserAccess(FieldAccess.readWrite);

	    ChildField portsField = new ChildField(Device.ports,
	    	Devices.Port,
	    	Cardinality.zeroOrMore);
	    setRelation(Device.ports, portsField);
	    portsField.setSystemAccess(FieldAccess.readWrite);
	    portsField.setUserAccess(FieldAccess.readWrite);
	}

	@Override
	protected DeviceModelObject createInstance(ModelId id, ModelId parentId) {
	    return new DeviceModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}