package com.nirmata.training.devices;

import java.util.List;
import javax.annotation.Generated;

import com.nirmata.bootstrap.*;

@Generated(value = { "com.nimata.modelgen" })
public class DevicesConfiguration implements ServiceConfiguration {
    @Override
    public List<ServiceModule> createModules() {
        List<ServiceModule> modules = ServiceModules.defaults(com.nirmata.training.devices.DevicesModelController.class);
        return modules;
    }
}