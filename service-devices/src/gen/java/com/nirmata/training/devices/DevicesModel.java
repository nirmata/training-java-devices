/**
 * Generated Model implementation for Devices
 */

package com.nirmata.training.devices;

import javax.annotation.Generated;
import java.util.*;

import com.nirmata.model.*;
import com.nirmata.model.alarms.*;
import com.nirmata.model.impl.AbstractModel;



@Generated(value = { "com.nimata.modelgen" })
public final class DevicesModel extends AbstractModel {

    DevicesModel() {
        super();
    }

    @Override
    public String getServiceId() {
        return "be601481-6d7a-40cc-8427-86d029049d25";
    }

    @Override
    public String getServiceName() {
        return "Devices";
    }

    @Override
    public ModelIndex getRootIndex() {
        return Devices.Root;
    }

    @Override
    public List <ModelIndex> getModelClasses() {
		List<ModelIndex> modelClasses = new ArrayList<ModelIndex>();
		modelClasses.addAll(Arrays.asList(BuiltIn.values()));
		modelClasses.addAll(Arrays.asList(Devices.values()));
		return modelClasses;
    }

    @Override
    public ModelClass getModelClass(ModelIndex modelIndex) { 
        String name = getServiceName();    
        if (modelIndex instanceof Devices) {
            Devices modelClass = (Devices) modelIndex;            
            switch (modelClass) {
                case Root:
                	return new RootModelClass(name);
                	
                case Device:
                	return new DeviceModelClass(name);
                	
                case DeviceConfig:
                	return new DeviceConfigModelClass(name);
                	
                case DeviceStatus:
                	return new DeviceStatusModelClass(name);
                	
                case Port:
                	return new PortModelClass(name);
                	
                case PortStatus:
                	return new PortStatusModelClass(name);
                	
                case PortStatistics:
                	return new PortStatisticsModelClass(name);
                	
                case LogRecord:
                	return new LogRecordModelClass(name);
                	
                default:
                    throw new java.lang.IllegalArgumentException(
                        "Invalid ModelIndex: " + modelIndex.getName());
            }
        } else if (modelIndex instanceof BuiltIn) {
            BuiltIn modelClass = (BuiltIn) modelIndex;
            switch (modelClass) {
                case AlarmType:
                	return new AlarmTypeModelClass(name, getRootIndex());
                	
                case Alarm:
                	return new AlarmModelClass(name, getRootIndex());
                	
                case AlarmCondition:
                	return new AlarmConditionModelClass(name, getRootIndex());
                	
                case AlarmNotifier:
                	return new AlarmNotifierModelClass(name, getRootIndex());
                	
                case AlarmScopeFilter:
                	return new AlarmScopeFilterModelClass(name, getRootIndex());
                	
                default:
                    throw new java.lang.IllegalArgumentException(
                        "Invalid BuiltIn ModelIndex:" + modelIndex.getName());
            }
        } else {
            throw new java.lang.IllegalArgumentException(
                "Invalid ModelIndex:" + modelIndex.getName()); 
        }
    }
}