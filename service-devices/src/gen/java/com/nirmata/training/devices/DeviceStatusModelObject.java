/**
 * Generated ModelObject: DeviceStatus
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class DeviceStatusModelObject extends AbstractModelObject {
    private DeviceAdministrativeState _administrativeState;
    private DeviceOperationalState _operationalState;

    DeviceStatusModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    }

    private DeviceStatusModelObject(DeviceStatusModelObject obj) {
    	super(obj);

    	_administrativeState = obj._administrativeState;
    	_operationalState = obj._operationalState;
    }

    @Override
    public DeviceStatusModelObject copy() {
    	return new DeviceStatusModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    DeviceStatus fields = (DeviceStatus) fieldIndex;
		switch (fields)
	    {
			case administrativeState:
			    return _administrativeState;

			case operationalState:
			    return _operationalState;

					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    DeviceStatus fields = (DeviceStatus) fieldIndex;
	    switch (fields) {
	        case administrativeState:
	            _administrativeState = (DeviceAdministrativeState) data;
	            break;
	        case operationalState:
	            _operationalState = (DeviceOperationalState) data;
	            break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    DeviceStatus fields = (DeviceStatus) fieldIndex;
	    switch (fields) {
					
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    DeviceStatus fields = (DeviceStatus) fieldIndex;
		switch (fields) {

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("DeviceStatus")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("administrativeState", _administrativeState)
	        .add("operationalState", _operationalState)
					        
	        .toString();
	}

}

