/**
 * Generated Enum: PortType
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;

import com.google.common.collect.ImmutableSet;

import com.nirmata.model.*;

@Generated(value = { "com.nimata.modelgen" })
public enum PortType implements ModelEnum {
    ether100mb,
    ether1gb;

    static Set<ModelEnum> _modelEnums;

    public static Set<ModelEnum> modelEnums() {
        if (_modelEnums == null) {
            ImmutableSet.Builder<ModelEnum> bldr = ImmutableSet.builder();
            for (ModelEnum e : PortType.values()) {
                bldr.add(e);
            }

            _modelEnums = bldr.build();
        }

        return _modelEnums;
    }

    @Override
    public Set<ModelEnum> getAll() {
        return _modelEnums;
    }

    @Override
    public String getName() {
        return name();
    }

    @Override
    public int getValue() {
        return ordinal();
    }
}