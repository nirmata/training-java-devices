/**
 * Generated ModelController for Devices
 */

package com.nirmata.training.devices;

import javax.annotation.Generated;

import com.google.inject.Inject;

import com.nirmata.store.ModelStore;
import com.nirmata.security.SecurityController;
import com.nirmata.model.ModelUpdater;
import com.nirmata.model.impl.AbstractModelController;
import com.nirmata.lock.LockService;

@Generated(value = { "com.nimata.modelgen" })
public final class DevicesModelController extends AbstractModelController {

	@Inject
    public DevicesModelController(ModelStore store, SecurityController security, LockService lockService) {
        super(new DevicesModel(), store, security, lockService);
    }

    @Inject(optional = true)
    public void updateModel(ModelUpdater updater) {
        _modelUpdater = updater;
    }    
}