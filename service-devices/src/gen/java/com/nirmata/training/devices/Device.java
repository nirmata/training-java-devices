/**
 * Generated ModelClassEnum: Device
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum Device implements FieldIndex {
    serialNumber,
    model,
    firmwareVersion,
    sysUpTime,
    config,
    status,
    ports;

    @Override
    public String getName() {
        return name();
    }  
}