/**
 * Generated ModelClassEnum: Root
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum Root implements FieldIndex {
    modelId,
    modelName,
    devices,
    logs,
    alarmTypes,
    alarmNotifiers;

    @Override
    public String getName() {
        return name();
    }  
}