/**
 * Generated ModelObject: PortStatus
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class PortStatusModelObject extends AbstractModelObject {
    private PortAdministrativeState _administrativeState;
    private PortOperationalState _operationalState;
    private String _diagnostic;

    PortStatusModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    }

    private PortStatusModelObject(PortStatusModelObject obj) {
    	super(obj);

    	_administrativeState = obj._administrativeState;
    	_operationalState = obj._operationalState;
    	_diagnostic = obj._diagnostic;
    }

    @Override
    public PortStatusModelObject copy() {
    	return new PortStatusModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    PortStatus fields = (PortStatus) fieldIndex;
		switch (fields)
	    {
			case administrativeState:
			    return _administrativeState;

			case operationalState:
			    return _operationalState;

			case diagnostic:
			    return _diagnostic;

					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    PortStatus fields = (PortStatus) fieldIndex;
	    switch (fields) {
	        case administrativeState:
	            _administrativeState = (PortAdministrativeState) data;
	            break;
	        case operationalState:
	            _operationalState = (PortOperationalState) data;
	            break;
	        case diagnostic:
	            _diagnostic = (String) data;
	            break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    PortStatus fields = (PortStatus) fieldIndex;
	    switch (fields) {
					
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    PortStatus fields = (PortStatus) fieldIndex;
		switch (fields) {

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("PortStatus")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("administrativeState", _administrativeState)
	        .add("operationalState", _operationalState)
	        .add("diagnostic", _diagnostic)
					        
	        .toString();
	}

}

