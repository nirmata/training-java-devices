/**
 * Generated ModelObject: PortStatistics
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class PortStatisticsModelObject extends AbstractModelObject {
    private Integer _bytesIn;
    private Integer _bytesOut;

    PortStatisticsModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    }

    private PortStatisticsModelObject(PortStatisticsModelObject obj) {
    	super(obj);

    	_bytesIn = obj._bytesIn;
    	_bytesOut = obj._bytesOut;
    }

    @Override
    public PortStatisticsModelObject copy() {
    	return new PortStatisticsModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    PortStatistics fields = (PortStatistics) fieldIndex;
		switch (fields)
	    {
			case bytesIn:
			    return _bytesIn;

			case bytesOut:
			    return _bytesOut;

					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    PortStatistics fields = (PortStatistics) fieldIndex;
	    switch (fields) {
	        case bytesIn:
	            _bytesIn = (Integer) data;
	            break;
	        case bytesOut:
	            _bytesOut = (Integer) data;
	            break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    PortStatistics fields = (PortStatistics) fieldIndex;
	    switch (fields) {
					
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    PortStatistics fields = (PortStatistics) fieldIndex;
		switch (fields) {

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("PortStatistics")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("bytesIn", _bytesIn)
	        .add("bytesOut", _bytesOut)
					        
	        .toString();
	}

}

