/**
 * Generated Model Servlet: 
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.servlet.AbstractModelServlet;
import com.nirmata.servlet.CorsPolicy;


@Generated(value = { "com.nimata.modelgen" })
public class DevicesServlet extends AbstractModelServlet {
    private static final long serialVersionUID = -1L;

    public DevicesServlet() {
        super();
        setCorsPolicy(CorsPolicy.UNRESTRICTED);      
    }

    @Override
    public String getServletName() {
        return "Devices";
    }

    @Override
    public String getUrl() {
        return "/api/*";
    }
}
