/**
 * Generated ModelClass: PortStatistics
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class PortStatisticsModelClass extends AbstractModelClass {
    PortStatisticsModelClass(String serviceName) {
        super(serviceName, Devices.PortStatistics);

    	setApiLabel("portStatistics");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Port);
    }

	protected void initializeAttributes() {
	    IntegerField bytesInField = new IntegerField(PortStatistics.bytesIn);
	    bytesInField.setRequired(true);
	    bytesInField.setLabel(false);
	    bytesInField.setUnique(UniqueScope.NONE);
	    bytesInField.setSystemAccess(FieldAccess.readWrite);
	    bytesInField.setUserAccess(FieldAccess.readWrite);
	    bytesInField.setConstraint("default", "0");
	    bytesInField.setConstraint("min", "0");
	    setAttribute(PortStatistics.bytesIn, bytesInField);

	    IntegerField bytesOutField = new IntegerField(PortStatistics.bytesOut);
	    bytesOutField.setRequired(true);
	    bytesOutField.setLabel(false);
	    bytesOutField.setUnique(UniqueScope.NONE);
	    bytesOutField.setSystemAccess(FieldAccess.readWrite);
	    bytesOutField.setUserAccess(FieldAccess.readCreate);
	    bytesOutField.setConstraint("default", "0");
	    bytesOutField.setConstraint("min", "0");
	    setAttribute(PortStatistics.bytesOut, bytesOutField);
	}

	protected void initializeRelations() {
	}

	@Override
	protected PortStatisticsModelObject createInstance(ModelId id, ModelId parentId) {
	    return new PortStatisticsModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}