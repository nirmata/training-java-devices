/**
 * Generated ModelObject: DeviceConfig
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class DeviceConfigModelObject extends AbstractModelObject {
    private Boolean _enableLogging;
    private Integer _logMaxSize;

    DeviceConfigModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    }

    private DeviceConfigModelObject(DeviceConfigModelObject obj) {
    	super(obj);

    	_enableLogging = obj._enableLogging;
    	_logMaxSize = obj._logMaxSize;
    }

    @Override
    public DeviceConfigModelObject copy() {
    	return new DeviceConfigModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    DeviceConfig fields = (DeviceConfig) fieldIndex;
		switch (fields)
	    {
			case enableLogging:
			    return _enableLogging;

			case logMaxSize:
			    return _logMaxSize;

					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    DeviceConfig fields = (DeviceConfig) fieldIndex;
	    switch (fields) {
	        case enableLogging:
	            _enableLogging = (Boolean) data;
	            break;
	        case logMaxSize:
	            _logMaxSize = (Integer) data;
	            break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    DeviceConfig fields = (DeviceConfig) fieldIndex;
	    switch (fields) {
					
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    DeviceConfig fields = (DeviceConfig) fieldIndex;
		switch (fields) {

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("DeviceConfig")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("enableLogging", _enableLogging)
	        .add("logMaxSize", _logMaxSize)
					        
	        .toString();
	}

}

