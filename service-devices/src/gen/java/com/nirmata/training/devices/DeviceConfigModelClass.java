/**
 * Generated ModelClass: DeviceConfig
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class DeviceConfigModelClass extends AbstractModelClass {
    DeviceConfigModelClass(String serviceName) {
        super(serviceName, Devices.DeviceConfig);

    	setApiLabel("deviceConfigs");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Device);
    }

	protected void initializeAttributes() {
	    BooleanField enableLoggingField = new BooleanField(DeviceConfig.enableLogging);
	    enableLoggingField.setRequired(true);
	    enableLoggingField.setLabel(false);
	    enableLoggingField.setUnique(UniqueScope.NONE);
	    enableLoggingField.setSystemAccess(FieldAccess.readWrite);
	    enableLoggingField.setUserAccess(FieldAccess.readWrite);
	    setAttribute(DeviceConfig.enableLogging, enableLoggingField);

	    IntegerField logMaxSizeField = new IntegerField(DeviceConfig.logMaxSize);
	    logMaxSizeField.setRequired(false);
	    logMaxSizeField.setLabel(false);
	    logMaxSizeField.setUnique(UniqueScope.NONE);
	    logMaxSizeField.setSystemAccess(FieldAccess.readWrite);
	    logMaxSizeField.setUserAccess(FieldAccess.readWrite);
	    logMaxSizeField.setConstraint("default", "10000");
	    logMaxSizeField.setConstraint("min", "0");
	    logMaxSizeField.setConstraint("max", "10000000");
	    setAttribute(DeviceConfig.logMaxSize, logMaxSizeField);
	}

	protected void initializeRelations() {
	}

	@Override
	protected DeviceConfigModelObject createInstance(ModelId id, ModelId parentId) {
	    return new DeviceConfigModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}