/**
 * Generated ModelClassEnum: DeviceConfig
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum DeviceConfig implements FieldIndex {
    enableLogging,
    logMaxSize;

    @Override
    public String getName() {
        return name();
    }  
}