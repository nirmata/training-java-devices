/**
 * Generated ModelClassEnum: PortStatistics
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum PortStatistics implements FieldIndex {
    bytesIn,
    bytesOut;

    @Override
    public String getName() {
        return name();
    }  
}