/**
 * Generated ModelClassEnum: PortStatus
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum PortStatus implements FieldIndex {
    administrativeState,
    operationalState,
    diagnostic;

    @Override
    public String getName() {
        return name();
    }  
}