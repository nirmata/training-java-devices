/**
 * Generated ModelObject: Port
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.google.common.base.MoreObjects;
import com.nirmata.model.*;
import com.nirmata.model.impl.*;

@Generated(value = { "com.nimata.modelgen" })
final class PortModelObject extends AbstractModelObject {
    private String _macAddress;
    private PortType _type;
    private Integer _portNumber;

	private ModelId _status;
	private ModelId _stats;

    PortModelObject(ModelId id, ModelId parentId) {
        super(id, parentId);

     
    	_status = null;
    	_stats = null;
    }

    private PortModelObject(PortModelObject obj) {
    	super(obj);

    	_macAddress = obj._macAddress;
    	_type = obj._type;
    	_portNumber = obj._portNumber;
    	_status = obj._status;	    
    	_stats = obj._stats;	    
    }

    @Override
    public PortModelObject copy() {
    	return new PortModelObject(this);
    }
	
	@Override
	public Object getValue(FieldIndex fieldIndex) {
	    Port fields = (Port) fieldIndex;
		switch (fields)
	    {
			case macAddress:
			    return _macAddress;

			case type:
			    return _type;

			case portNumber:
			    return _portNumber;

			case status:
			    return _status;

			case stats:
			    return _stats;
					
	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void setValue(FieldIndex fieldIndex, Object data) {
	    Port fields = (Port) fieldIndex;
	    switch (fields) {
	        case macAddress:
	            _macAddress = (String) data;
	            break;
	        case type:
	            _type = (PortType) data;
	            break;
	        case portNumber:
	            _portNumber = (Integer) data;
	            break;
	        case status:
	            _status = (ModelId) data;
	        	break;

	        case stats:
	            _stats = (ModelId) data;
	        	break;
			
	        default:
	            throw super.illegalFieldException(fieldIndex);
	    }
	}
	
	@Override
	public void addRelation(FieldIndex fieldIndex, ModelId relationId) {
	    Port fields = (Port) fieldIndex;
	    switch (fields) {
		case status:
		    _status = relationId;
		    break;
		case stats:
		    _stats = relationId;
		    break;				
	        default:
	            throw super.illegalFieldException(fieldIndex);
		}
	}
	
	@Override
	public void removeRelation(FieldIndex fieldIndex, ModelId relationId) {    
	    Port fields = (Port) fieldIndex;
		switch (fields) {
			case status: {
			    if (_status != null && _status.equals(relationId)) {
			        _status = null;
			    }
			    break;
			}

			case stats: {
			    if (_stats != null && _stats.equals(relationId)) {
			        _stats = null;
			    }
			    break;
			}

	        default:
				throw super.illegalFieldException(fieldIndex);
		}
	}

	@Override
	public String toString() {
	    return MoreObjects.toStringHelper("Port")
	        .add("id", getId())
	        .add("labels", getLabels())
	        .add("macAddress", _macAddress)
	        .add("type", _type)
	        .add("portNumber", _portNumber)
			.add("status", _status)
			.add("stats", _stats)				        
	        .toString();
	}

}

