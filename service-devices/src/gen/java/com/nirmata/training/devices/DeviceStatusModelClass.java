/**
 * Generated ModelClass: DeviceStatus
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class DeviceStatusModelClass extends AbstractModelClass {
    DeviceStatusModelClass(String serviceName) {
        super(serviceName, Devices.DeviceStatus);

    	setApiLabel("deviceStatuses");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Device);
    }

	protected void initializeAttributes() {
	    EnumField administrativeStateField = new EnumField(DeviceStatus.administrativeState, DeviceAdministrativeState.modelEnums());
	    administrativeStateField.setRequired(true);
	    administrativeStateField.setLabel(false);
	    administrativeStateField.setUnique(UniqueScope.NONE);
	    administrativeStateField.setSystemAccess(FieldAccess.readWrite);
	    administrativeStateField.setUserAccess(FieldAccess.readWrite);
	    setAttribute(DeviceStatus.administrativeState, administrativeStateField);

	    EnumField operationalStateField = new EnumField(DeviceStatus.operationalState, DeviceOperationalState.modelEnums());
	    operationalStateField.setRequired(true);
	    operationalStateField.setLabel(false);
	    operationalStateField.setUnique(UniqueScope.NONE);
	    operationalStateField.setSystemAccess(FieldAccess.readWrite);
	    operationalStateField.setUserAccess(FieldAccess.readCreate);
	    setAttribute(DeviceStatus.operationalState, operationalStateField);
	}

	protected void initializeRelations() {
	}

	@Override
	protected DeviceStatusModelObject createInstance(ModelId id, ModelId parentId) {
	    return new DeviceStatusModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}