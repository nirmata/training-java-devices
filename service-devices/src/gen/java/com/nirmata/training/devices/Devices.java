/**
 * Generated ModelEnum: 
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.ModelIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum Devices implements ModelIndex {
    Root,
    Device,
    DeviceConfig,
    DeviceStatus,
    Port,
    PortStatus,
    PortStatistics,
    LogRecord;

    @Override
    public String getName() {
        return name();
    } 
}