/**
 * Generated ModelClass: Root
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class RootModelClass extends AbstractModelClass {
    RootModelClass(String serviceName) {
        super(serviceName, Devices.Root);

    	setApiLabel("roots");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of();
    }

	protected void initializeAttributes() {
	    StringField modelIdField = new StringField(Root.modelId);
	    modelIdField.setRequired(false);
	    modelIdField.setLabel(false);
	    modelIdField.setUnique(UniqueScope.NONE);
	    modelIdField.setSystemAccess(FieldAccess.readCreate);
	    modelIdField.setUserAccess(FieldAccess.readCreate);
	    modelIdField.setConstraint("default", "be601481-6d7a-40cc-8427-86d029049d25");
	    setAttribute(Root.modelId, modelIdField);

	    StringField modelNameField = new StringField(Root.modelName);
	    modelNameField.setRequired(false);
	    modelNameField.setLabel(false);
	    modelNameField.setUnique(UniqueScope.NONE);
	    modelNameField.setSystemAccess(FieldAccess.readCreate);
	    modelNameField.setUserAccess(FieldAccess.readCreate);
	    modelNameField.setConstraint("default", "Devices");
	    setAttribute(Root.modelName, modelNameField);
	}

	protected void initializeRelations() {
	    ChildField devicesField = new ChildField(Root.devices,
	    	Devices.Device,
	    	Cardinality.zeroOrMore);
	    setRelation(Root.devices, devicesField);
	    devicesField.setSystemAccess(FieldAccess.readWrite);
	    devicesField.setUserAccess(FieldAccess.readWrite);

	    ChildField logsField = new ChildField(Root.logs,
	    	Devices.LogRecord,
	    	Cardinality.zeroOrMore);
	    setRelation(Root.logs, logsField);
	    logsField.setSystemAccess(FieldAccess.readWrite);
	    logsField.setUserAccess(FieldAccess.readWrite);

	    ChildField alarmTypesField = new ChildField(Root.alarmTypes,
	    	BuiltIn.AlarmType,
	    	Cardinality.zeroOrMore);
	    setRelation(Root.alarmTypes, alarmTypesField);
	    alarmTypesField.setSystemAccess(FieldAccess.readWrite);
	    alarmTypesField.setUserAccess(FieldAccess.readWrite);

	    ChildField alarmNotifiersField = new ChildField(Root.alarmNotifiers,
	    	BuiltIn.AlarmNotifier,
	    	Cardinality.zeroOrMore);
	    setRelation(Root.alarmNotifiers, alarmNotifiersField);
	    alarmNotifiersField.setSystemAccess(FieldAccess.readWrite);
	    alarmNotifiersField.setUserAccess(FieldAccess.readWrite);
	}

	@Override
	protected RootModelObject createInstance(ModelId id, ModelId parentId) {
	    return new RootModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}