/**
 * Generated ModelClass: LogRecord
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class LogRecordModelClass extends AbstractModelClass {
    LogRecordModelClass(String serviceName) {
        super(serviceName, Devices.LogRecord);

    	setApiLabel("logRecords");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Root);
    }

	protected void initializeAttributes() {
	    StringField messageField = new StringField(LogRecord.message);
	    messageField.setRequired(true);
	    messageField.setLabel(false);
	    messageField.setUnique(UniqueScope.NONE);
	    messageField.setSystemAccess(FieldAccess.readWrite);
	    messageField.setUserAccess(FieldAccess.readWrite);
	    messageField.setConstraint("length", "255");
	    setAttribute(LogRecord.message, messageField);

	    LongField timestampField = new LongField(LogRecord.timestamp);
	    timestampField.setRequired(true);
	    timestampField.setLabel(false);
	    timestampField.setUnique(UniqueScope.NONE);
	    timestampField.setSystemAccess(FieldAccess.readWrite);
	    timestampField.setUserAccess(FieldAccess.readWrite);
	    timestampField.setConstraint("min", "0");
	    setAttribute(LogRecord.timestamp, timestampField);
	}

	protected void initializeRelations() {
	}

	@Override
	protected LogRecordModelObject createInstance(ModelId id, ModelId parentId) {
	    return new LogRecordModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}