/**
 * Generated ModelClassEnum: DeviceStatus
 */
package com.nirmata.training.devices;

import javax.annotation.Generated;
import com.nirmata.model.FieldIndex;

@Generated(value = { "com.nimata.modelgen" })
public enum DeviceStatus implements FieldIndex {
    administrativeState,
    operationalState;

    @Override
    public String getName() {
        return name();
    }  
}