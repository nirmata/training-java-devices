/**
 * Generated ModelClass: Port
 */
package com.nirmata.training.devices;

import java.util.Set;
import javax.annotation.Generated;
import com.google.common.collect.ImmutableSet;
import com.nirmata.model.*;
import com.nirmata.model.field.*;
import com.nirmata.model.impl.AbstractModelClass;

@Generated(value = { "com.nimata.modelgen" })
final class PortModelClass extends AbstractModelClass {
    PortModelClass(String serviceName) {
        super(serviceName, Devices.Port);

    	setApiLabel("ports");
    	initializeAttributes();
    	initializeRelations();
    }

    @Override
    public Set<? extends ModelIndex> getParents() {
        return ImmutableSet.of(Devices.Device);
    }

	protected void initializeAttributes() {
	    StringField macAddressField = new StringField(Port.macAddress);
	    macAddressField.setRequired(true);
	    macAddressField.setLabel(false);
	    macAddressField.setUnique(UniqueScope.GLOBAL);
	    macAddressField.setSystemAccess(FieldAccess.readWrite);
	    macAddressField.setUserAccess(FieldAccess.readCreate);
	    macAddressField.setConstraint("length", "17");
	    macAddressField.setConstraint("allowedText", "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$");
	    macAddressField.setConstraint("default", "00:00:00:00:00");
	    setAttribute(Port.macAddress, macAddressField);

	    EnumField typeField = new EnumField(Port.type, PortType.modelEnums());
	    typeField.setRequired(true);
	    typeField.setLabel(false);
	    typeField.setUnique(UniqueScope.NONE);
	    typeField.setSystemAccess(FieldAccess.readWrite);
	    typeField.setUserAccess(FieldAccess.readWrite);
	    setAttribute(Port.type, typeField);

	    IntegerField portNumberField = new IntegerField(Port.portNumber);
	    portNumberField.setRequired(true);
	    portNumberField.setLabel(false);
	    portNumberField.setUnique(UniqueScope.PARENT);
	    portNumberField.setSystemAccess(FieldAccess.readWrite);
	    portNumberField.setUserAccess(FieldAccess.readCreate);
	    portNumberField.setConstraint("max", "8");
	    portNumberField.setConstraint("min", "1");
	    setAttribute(Port.portNumber, portNumberField);
	}

	protected void initializeRelations() {
	    ChildField statusField = new ChildField(Port.status,
	    	Devices.PortStatus,
	    	Cardinality.one);
	    setRelation(Port.status, statusField);
	    statusField.setSystemAccess(FieldAccess.readWrite);
	    statusField.setUserAccess(FieldAccess.readWrite);

	    ChildField statsField = new ChildField(Port.stats,
	    	Devices.PortStatistics,
	    	Cardinality.one);
	    setRelation(Port.stats, statsField);
	    statsField.setSystemAccess(FieldAccess.readWrite);
	    statsField.setUserAccess(FieldAccess.readWrite);
	}

	@Override
	protected PortModelObject createInstance(ModelId id, ModelId parentId) {
	    return new PortModelObject(id, parentId);
	}

	@Override
	public boolean isUserAccessible() {
	    return true;
	}

	@Override
	public boolean isSystemAccessible() {
	    return true;
	}

	@Override
	public boolean isUserDeleteable() {
	    return true;
	}

	@Override
	public boolean isSystemDeleteable() {
	    return true;
	}

	@Override
	public boolean inheritLabels() {
	    return false;
	}
    
}