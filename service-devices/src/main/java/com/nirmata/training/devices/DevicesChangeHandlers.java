package com.nirmata.training.devices;

import com.nirmata.model.ModelController;
import com.nirmata.training.devices.defaults.DeviceDefaults;
import com.nirmata.training.devices.defaults.PortDefaults;
import com.nirmata.training.devices.defaults.RootDefaults;
import com.nirmata.training.devices.handlers.LogRecordHandlers;
import com.nirmata.training.devices.handlers.PortStatisticsHandler;
import com.nirmata.training.devices.validate.ChangeValidator;

public class DevicesChangeHandlers {

    public static void registerHandlers(ModelController ctrl) {

        // ctrl.register(new LogRecordHandlers());
        ctrl.register(new RootDefaults());
        ctrl.register(new DeviceDefaults());
        ctrl.register(new PortDefaults());
        ctrl.register(new PortStatisticsHandler(ctrl));
        ctrl.register(new ChangeValidator());

    }
}
