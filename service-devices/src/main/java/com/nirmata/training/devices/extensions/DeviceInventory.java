package com.nirmata.training.devices.extensions;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelId;
import com.nirmata.model.ModelObject;
import com.nirmata.training.devices.Device;
import com.nirmata.training.devices.Root;

public class DeviceInventory {

    private static final Logger _logger = LoggerFactory.getLogger(DeviceInventory.class);

    public static Map<String, Object> export(ModelController ctrl, ModelObject root,
        HttpServletRequest req) {

        try {
            Map<String, Object> output = new HashMap<String,Object>();
            
            root.getRelations(Root.devices).forEach(deviceId -> {
            	ModelObject device = ctrl.getModelObject(deviceId);
            	String sn = device.get(Device.serialNumber);
            	String version = device.get(Device.firmwareVersion);
            	output.put(sn,version);
            });
            
            return ImmutableMap.of(
                "status", 200,
                "message", "Success",
                "inventory", output);

        } catch (Throwable t) {

            _logger.error("Failed to perform device inventory ", t);
            return ImmutableMap.of(
                "status", 500,
                "message", "Server error (" + t.getMessage() + ")");
        }
    }


}
