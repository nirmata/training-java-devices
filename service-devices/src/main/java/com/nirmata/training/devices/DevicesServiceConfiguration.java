package com.nirmata.training.devices;

import java.util.List;

import com.nirmata.bootstrap.*;
import com.nirmata.training.devices.audittrail.DevicesNotificationModule;

public class DevicesServiceConfiguration implements ServiceConfiguration {

    @Override
    public List<ServiceModule> createModules() {

        ServiceModules modules = new ServiceModules();
        modules.add(new DevicesRestApi());
        modules.add(new DevicesModules());
        modules.add(new DevicesNotificationModule());
        return modules.get();
    }
}