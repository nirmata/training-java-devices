package com.nirmata.training.devices.validate;

import java.util.List;

import com.nirmata.model.ChangeSet;
import com.nirmata.model.ModelId;
import com.nirmata.model.ModelObject;
import com.nirmata.model.ModelRepository;
import com.nirmata.model.ValidationError;
import com.nirmata.model.navigate.ModelNavigator;
import com.nirmata.training.devices.Device;
import com.nirmata.training.devices.Devices;
import com.nirmata.validate.impl.FieldValidationError;

public class DeviceValidator {

	

    public static List<ValidationError> validate(ModelNavigator nav, ModelRepository repo, ChangeSet changeSet,
        ModelRepository txnRepo, List<ValidationError> errors) {
   
        for (ModelId deviceId : changeSet.getCreatedIds(Devices.Device)) {
        	validateDevice(deviceId, nav, errors);
        }
        return errors;
    }
    
    private static void validateDevice(ModelId deviceId, ModelNavigator nav, List<ValidationError> errors) {
    	ModelObject device  = nav.find(deviceId);
    	int numberOfPorts = device.getRelations(Device.ports).size();
    	if (numberOfPorts == 8)  {
    		return;
    	}
    	
    	String sn = device.get(Device.serialNumber);
    	String msg = "Found "+numberOfPorts+" ports for Device "+sn+". Expected 8 ports";
        ValidationError error = new FieldValidationError(
                ValidationErrorCodes.InvalidConfiguration.name(), deviceId,
                Device.ports, msg);
        errors.add(error);
    }
	
}
