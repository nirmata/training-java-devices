package com.nirmata.training.devices.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;
import com.nirmata.client.ChangeNotification;
import com.nirmata.model.ChangeSet;
import com.nirmata.model.FieldIndex;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelId;
import com.nirmata.model.ModelRepository;
import com.nirmata.model.ModelTransaction;
import com.nirmata.notify.PostCommitNotify;
import com.nirmata.notify.PostCreateNotify;
import com.nirmata.notify.PostDeleteNotify;
import com.nirmata.notify.PostModifyNotify;
import com.nirmata.notify.PreCommitNotify;
import com.nirmata.notify.PreCreateNotify;
import com.nirmata.notify.PreDeleteNotify;
import com.nirmata.notify.PreValidateNotify;


public class LogRecordHandlers {

    private final Logger _logger = LoggerFactory.getLogger(getClass());
	
    @PreCreateNotify("LogRecord")
    public void preCreateNotify(ModelTransaction txn, ModelId id) {
    	log("@PreCreateNotify",txn,id);
    }
    
    @PostCreateNotify("LogRecord")
    public void postCreateNotify(ModelTransaction txn, ModelId id) {
    	log("@PostCreateNotify",txn,id);
    }
    
    @PostModifyNotify("LogRecord")
    public void postModifyNotify(ModelTransaction txn, ModelId id, FieldIndex fieldIndex) {
    	log("@PostCreateNotify",txn,id,fieldIndex);
    }
    
    @PreDeleteNotify("LogRecord")
    public void preDeleteNotify(ModelTransaction txn, ModelId id) {
    	log("@PreDeleteNotify",txn,id);
    }
    
    @PostDeleteNotify("LogRecord")
    public void postDeleteNotify(ModelTransaction txn, ModelId id) {
    	log("@PostDeleteNotify",txn,id);
    }
    
    @PreValidateNotify
    public void preValidateNotify(ModelTransaction txn, ChangeSet changeSet) {
    	log("@PreValidateNotify",txn);
    }
    
    @PreCommitNotify
    public void preCommitNotify(ModelRepository repo, ChangeNotification changeSet) {
    	log("@PreCommitNotify");
    }
    
    @PostCommitNotify
    public void portCommitNotify(ChangeNotification changes) {
    	log("@PostCommitNotify");
    }
    
    @Subscribe
    public void subscribe(ModelRepository repo, ChangeNotification changes) {
    	log("@Subscribe");
    }
    
    private void log(String annotation, ModelTransaction txn, ModelId id) {
    	Long threadId = Thread.currentThread().getId();
    	String modelIndex = id.getModelIndex().getName();
    	String uuid = id.getUuid();
    	String msg = threadId+" - "+annotation+ " "+modelIndex+" "+uuid;
    	_logger.debug("{}",msg);
    }
    
    private void log(String annotation, ModelTransaction txn) {
    	Long threadId = Thread.currentThread().getId();
    	String msg = threadId+" - "+annotation;;
    	_logger.debug("{}",msg);
    }
    
    private void log(String annotation) {
    	Long threadId = Thread.currentThread().getId();
    	String msg = threadId+" - "+annotation;;
    	_logger.debug("{}",msg);
    }
    
    private void log(String annotation, ModelTransaction txn, ModelId id, FieldIndex index) {
    	Long threadId = Thread.currentThread().getId();
    	String modelIndex = id.getModelIndex().getName();
    	String uuid = id.getUuid();
    	String msg = threadId+" - "+annotation+ " "+modelIndex+" "+uuid;
    	msg += " "+index.getName();
    	_logger.debug("{}",msg);
    }
	
}
