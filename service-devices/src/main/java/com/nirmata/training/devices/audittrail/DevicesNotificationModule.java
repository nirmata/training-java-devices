/**
 * Copyright Nirmata 2014
 */

package com.nirmata.training.devices.audittrail;

import com.nirmata.training.devices.audittrail.DevicesAuditTrailProducer;
import com.nirmata.notification.model.ModelNotificationProducer;
import com.nirmata.notification.model.NotificationModule;

public class DevicesNotificationModule extends NotificationModule {

    @Override
    protected Class<? extends ModelNotificationProducer> getNotificationProducer() {
        return DevicesAuditTrailProducer.class;
    }

}
