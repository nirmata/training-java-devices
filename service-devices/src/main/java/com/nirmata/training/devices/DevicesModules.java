package com.nirmata.training.devices;

import javax.servlet.ServletConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.nirmata.bootstrap.ServiceModule;
import com.nirmata.lock.LockService;
import com.nirmata.lock.NullLockService;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelUpdater;
import com.nirmata.security.SecurityController;
import com.nirmata.security.impl.DefaultSecurityController;
import com.nirmata.store.MapModelStore;
import com.nirmata.store.ModelStore;
import com.nirmata.training.devices.extensions.DevicesRestExtensions;
import com.nirmata.training.updates.DevicesUpdater;


public class DevicesModules extends AbstractModule implements ServiceModule {

    private static Logger _logger = LoggerFactory.getLogger(DevicesModules.class);
    private static ModelController _ctrl;

    public static class Entry {


        @Inject
        public Entry(ServletConfig config, ModelController ctrl) {
            _logger.debug("In DevicesModules.Entry for {}", config.getServletName());
            _ctrl = ctrl;
        }

        @Inject
        void initializeHandlers(ModelController ctrl) {
            DevicesChangeHandlers.registerHandlers(ctrl);
        }
    }

    @Override
    public Class<?> getEntryPoint() {
        return DevicesModules.Entry.class;
    }

    @Override
    public void configure() {

        ModelStore mapStore = new MapModelStore();
        bind(ModelStore.class).toInstance(mapStore);

        bind(LockService.class).toInstance(NullLockService.INSTANCE);
        bind(ModelController.class).to(DevicesModelController.class).asEagerSingleton();
        bind(DevicesRestExtensions.class).asEagerSingleton();

        bind(ModelUpdater.class).to(DevicesUpdater.class);

        final DefaultSecurityController sctrl = DefaultSecurityController.DISABLED;
        sctrl.setEntityName("devices");

        bind(SecurityController.class).toInstance(sctrl);
        bind(DevicesModules.Entry.class);
    }


    @Override
    public void open(Object entryPoint) {
        Preconditions.checkNotNull(_ctrl);
        _ctrl.initializeModel();
    }

    @Override
    public void start() {
    }

    @Override
    public void close() throws Exception {
    }
}