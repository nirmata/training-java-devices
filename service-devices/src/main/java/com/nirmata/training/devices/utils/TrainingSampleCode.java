package com.nirmata.training.devices.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nirmata.model.Model;
import com.nirmata.model.ModelClass;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelField;
import com.nirmata.model.ModelId;
import com.nirmata.model.ModelObject;
import com.nirmata.model.ModelTransaction;
import com.nirmata.model.navigate.ModelNavigator;
import com.nirmata.model.navigate.ModelPath;
import com.nirmata.model.navigate.Navigators;
import com.nirmata.training.devices.Device;
import com.nirmata.training.devices.Port;
import com.nirmata.training.devices.PortAdministrativeState;
import com.nirmata.training.devices.PortStatus;

public class TrainingSampleCode {
    private final static Logger _logger = LoggerFactory.getLogger(TrainingSampleCode.class);

    public static void inspectAllModelClasses(ModelTransaction txn) {
    	Model model = txn.getModel();
    	
    	model.getModelClasses().forEach(modelIndex -> {
    		ModelClass modelClass = model.getModelClass(modelIndex);
    		String modelClassName = modelIndex.getName();
    		_logger.debug("inspecting ModelClass {}",modelClassName);
    		
    		modelClass.getFields().forEach(fieldIndex->{
    			ModelField field = modelClass.getField(fieldIndex);
    			String type = field.getTypeName();
    			String name = field.getName();
    			_logger.debug("type of attribute {} is {}",name,type);;
    		});
    	});
    }
    
    
    public static List<ModelObject> getDisabledPorts(ModelController ctrl, ModelId deviceId) {
            ModelNavigator nav = Navigators.navigator(ctrl);
            List<ModelObject> ports = new ArrayList<>();
            ModelPath path = ModelPath.build(Device.ports, Port.status);
            nav.forEach(deviceId, path, (status) -> {

                if (status.get(PortStatus.administrativeState) == PortAdministrativeState.disabled) {
                    ports.add(nav.parent(status));
                }
            });
            return ports;
    }
}
