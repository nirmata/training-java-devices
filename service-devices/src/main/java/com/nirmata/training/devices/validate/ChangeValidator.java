package com.nirmata.training.devices.validate;

import java.util.ArrayList;
import java.util.List;

import com.nirmata.model.ChangeSet;
import com.nirmata.model.ModelRepository;
import com.nirmata.model.ValidationError;
import com.nirmata.model.navigate.ModelNavigator;
import com.nirmata.model.navigate.Navigators;
import com.nirmata.notify.Validate;

public class ChangeValidator {

    @Validate
    public List<ValidationError> validate(ModelRepository baseRepo, ChangeSet changeSet, ModelRepository txnRepo) {
        List<ValidationError> errors = new ArrayList<>();
        ModelNavigator nav = Navigators.changeSetNavigator(changeSet, baseRepo);

        DeviceValidator.validate(nav, baseRepo, changeSet, txnRepo, errors);


        return errors;
    }
}