package com.nirmata.training.devices.validate;

public enum ValidationErrorCodes {
    InvalidConfiguration,
    InvalidCredentials,
    NotDeleteable,
    InvalidFormat,
    RelationNotNull,
    InvalidStateTransition,
    InUse,
    IncompatibleVersion,
    LimitExceeded,
    NoMatchingPort,
    MissingValue,
    NotReady
}
