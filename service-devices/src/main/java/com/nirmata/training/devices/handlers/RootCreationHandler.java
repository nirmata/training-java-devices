package com.nirmata.training.devices.handlers;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelId;
import com.nirmata.model.ModelObject;
import com.nirmata.model.ModelTransaction;
import com.nirmata.model.ValidationException;
import com.nirmata.notify.ChangeNotification;
import com.nirmata.security.SecurityController;
import com.nirmata.training.devices.Device;
import com.nirmata.training.devices.DeviceModel;
import com.nirmata.training.devices.Devices;
import com.nirmata.training.devices.Port;
import com.nirmata.training.devices.PortAdministrativeState;
import com.nirmata.training.devices.PortOperationalState;
import com.nirmata.training.devices.PortStatus;
import com.nirmata.training.devices.PortType;
import com.nirmata.training.devices.Root;

public class RootCreationHandler {

    private final Logger _logger;
    private final SecurityController _sctrl;
    private final ModelController _ctrl;

    public RootCreationHandler(ModelController ctrl) {
        _sctrl = ctrl.getSecurityController();
        _logger = LoggerFactory.getLogger(getClass());
        _ctrl = ctrl;

    }

    @Subscribe
    public void populateTestDevices(ChangeNotification changes) {
        try {
            _sctrl.loginLocal();
            String sid = changes.getSequenceId();
            Set<ModelId> rootIds = changes.getCreatedIds(Devices.Root);
            for (ModelId rootId : rootIds) {
                _logger.debug("Create Devices for Root {}", rootId.getUuid());
                createDevices(sid, rootId);
            }
        } catch (Exception e) {
            _logger.error("Failed to execute createApplications: ", e);
        } finally {
            _sctrl.logout();
        }
    }
    
    private void createDevices(String sid, ModelId rootId) throws ValidationException {
    	try (ModelTransaction txn = _ctrl.newTransaction(sid)) {
    		txn.start();
    		createDevice(txn,rootId,DeviceModel.acme100,"sn-000001");
    		createDevice(txn,rootId,DeviceModel.acme200,"sn-000002");
    		txn.commit();
    	}
    }
    
    private void createDevice(
    		ModelTransaction txn, 
    		ModelId rootId,
    		DeviceModel model,
    		String sn) {
    	
    	ModelObject device = txn.createObject(rootId, Root.devices);
    	ModelId deviceId = device.getId();
    	
    	txn.modify(deviceId, Device.firmwareVersion, "1.0");
    	txn.modify(deviceId, Device.model, model);
    	txn.modify(deviceId, Device.serialNumber, sn);
    	
    	createPorts(txn,deviceId);
    }
    
    private void createPorts(ModelTransaction txn, ModelId deviceId) {
    	for (int i=1;i<=8;i++) {
    		ModelObject port = txn.createObject(deviceId, Device.ports);
    		ModelId portId = port.getId();
    		
    		txn.modify(portId, Port.portNumber, i);
    		txn.modify(portId, Port.macAddress, generateRandomMacAddress());
    		txn.modify(portId, Port.type, PortType.ether1gb);
    		
    		createPortStatus(txn,portId);
    	}
    }
    
    private ModelId createPortStatus(ModelTransaction txn, ModelId portId) {
    	ModelObject status  = txn.createObject(portId, Port.status);
    	txn.modify(status.getId(), PortStatus.administrativeState, PortAdministrativeState.enabled);
    	txn.modify(status.getId(), PortStatus.operationalState, PortOperationalState.down);
    	return status.getId();
    }
    
    private String generateRandomMacAddress() {
    	return "AA:BB:CC:DD:EE:FF";
    }
}