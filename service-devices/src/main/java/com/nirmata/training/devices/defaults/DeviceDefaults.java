package com.nirmata.training.devices.defaults;

import com.nirmata.model.*;
import com.nirmata.notify.PreValidateNotify;
import com.nirmata.training.devices.*;

public class DeviceDefaults {

	    @PreValidateNotify
	    public void deviceDefaults(ModelTransaction txn, ChangeSet changeSet) {

	        for (ModelId deviceId : changeSet.getCreatedIds(Devices.Device)) {
	            createDeviceStatus(txn, deviceId);
	            createDeviceConfig(txn, deviceId);
	        }
	    }

	    private void createDeviceStatus(ModelTransaction txn, ModelId deviceId) {
	        ModelObject device = txn.getModelObject(deviceId);
	        ModelId statusId = device.getRelation(Device.status);
	        if (statusId == null) {
	            ModelObject status = txn.createObject(deviceId, Device.status);
	            statusId = status.getId();
	            txn.modify(statusId, DeviceStatus.administrativeState, DeviceAdministrativeState.enabled);
	            txn.modify(statusId, DeviceStatus.operationalState, DeviceOperationalState.down);
	        }
	    }
	    
	    private void createDeviceConfig(ModelTransaction txn, ModelId deviceId) {
	        ModelObject device = txn.getModelObject(deviceId);
	        ModelId configId = device.getRelation(Device.config);
	        if (configId == null) {
	            ModelObject config = txn.createObject(deviceId, Device.config);
	            configId = config.getId();
	            txn.modify(configId, DeviceConfig.enableLogging, true);
	            txn.modify(configId, DeviceConfig.logMaxSize, 10000);
	        }
	    }
}