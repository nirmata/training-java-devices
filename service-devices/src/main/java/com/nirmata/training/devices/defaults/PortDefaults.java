package com.nirmata.training.devices.defaults;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nirmata.model.*;
import com.nirmata.notify.PreValidateNotify;
import com.nirmata.training.devices.*;

public class PortDefaults {
	  @SuppressWarnings("unused")
	private static Logger _logger = LoggerFactory.getLogger(RootDefaults.class);


	    @PreValidateNotify
	    public void portDefaults(ModelTransaction txn, ChangeSet changeSet) {

	        for (ModelId portId : changeSet.getCreatedIds(Devices.Port)) {
	            createPortStatus(txn, portId);
	            createPortStatistics(txn, portId);
	        }
	    }

	    private void createPortStatus(ModelTransaction txn, ModelId portId) {
	        ModelObject port = txn.getModelObject(portId);
	        ModelId statusId = port.getRelation(Port.status);
	        if (statusId == null) {
	            ModelObject status = txn.createObject(portId, Port.status);
	            statusId = status.getId();
	            txn.modify(statusId, PortStatus.administrativeState, PortAdministrativeState.enabled);
	            txn.modify(statusId, PortStatus.operationalState, PortOperationalState.down);
	        }
	    }
	    
	    private void createPortStatistics(ModelTransaction txn, ModelId portId) {
	        ModelObject port = txn.getModelObject(portId);
	        ModelId statsId = port.getRelation(Port.stats);
	        if (statsId == null) {
	            txn.createObject(portId, Port.stats);
	        }
	    }
}