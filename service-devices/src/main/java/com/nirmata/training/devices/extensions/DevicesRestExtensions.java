package com.nirmata.training.devices.extensions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.nirmata.model.ModelController;
import com.nirmata.servlet.ext.DefaultRestApiExtensions;
import com.nirmata.training.devices.Devices;

public class DevicesRestExtensions {

    private static final Logger _logger = LoggerFactory.getLogger(DevicesRestExtensions.class);
    private final DefaultRestApiExtensions _extensions;

    @Inject
    public DevicesRestExtensions(ModelController ctrl, DefaultRestApiExtensions extensions) {

        _logger.debug("Initialize DevicesRestExtensions");
        _extensions = extensions;
    }

    @Inject
    public void start() {
        _logger.debug("Start DevicesRestExtensions");
        
        _extensions.register("GET", Devices.Root, "/inventory", (ctrl, mo, req) -> {
            return DeviceInventory.export(ctrl, mo, req);
        });


    }
}