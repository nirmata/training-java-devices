package com.nirmata.training.devices.defaults;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nirmata.model.*;
import com.nirmata.notify.PostCreateNotify;
import com.nirmata.training.devices.*;

public class RootDefaults {
	  private static Logger _logger = LoggerFactory.getLogger(RootDefaults.class);

    @PostCreateNotify("Root")
    public void rootDefaults(ModelTransaction txn, ModelId rootId) throws ValidationException {
    	_logger.debug("rootDefaults");
    	createTestDevices(txn,rootId);
    }
    
    private void createTestDevices(ModelTransaction txn, ModelId rootId) throws ValidationException {
    	    if (txn.getModelObject(rootId).getRelations(Root.devices).size() > 0) {
    	    	return;
    	    }
    		createDevice(txn,rootId,DeviceModel.acme100,"sn-000001");
    		createDevice(txn,rootId,DeviceModel.acme200,"sn-000002");
    }
    
    private void createDevice(
    		ModelTransaction txn, 
    		ModelId rootId,
    		DeviceModel model,
    		String sn) {
    	
    	ModelObject device = txn.createObject(rootId, Root.devices);
    	ModelId deviceId = device.getId();
    	
    	txn.modify(deviceId, Device.firmwareVersion, "1.0");
    	txn.modify(deviceId, Device.model, model);
    	txn.modify(deviceId, Device.serialNumber, sn);
    	
    	createPorts(txn,deviceId);
    }
    
    private void createPorts(ModelTransaction txn, ModelId deviceId) {
    	for (int i=1;i<=8;i++) {
    		ModelObject port = txn.createObject(deviceId, Device.ports);
    		ModelId portId = port.getId();
    		
    		txn.modify(portId, Port.portNumber, i);
    		txn.modify(portId, Port.macAddress, randomMACAddress());
    		txn.modify(portId, Port.type, PortType.ether1gb);
    		
    		createPortStatus(txn,portId);
    	}
    }
    
    private ModelId createPortStatus(ModelTransaction txn, ModelId portId) {
    	ModelObject status  = txn.createObject(portId, Port.status);
    	txn.modify(status.getId(), PortStatus.administrativeState, PortAdministrativeState.enabled);
    	txn.modify(status.getId(), PortStatus.operationalState, PortOperationalState.down);
    	return status.getId();
    }
    
  
    private String randomMACAddress(){
        Random rand = new Random();
        byte[] macAddr = new byte[6];
        rand.nextBytes(macAddr);

        macAddr[0] = (byte)(macAddr[0] & (byte)254);  //zeroing last 2 bytes to make it unicast and locally administrated

        StringBuilder sb = new StringBuilder(18);
        for(byte b : macAddr){

            if(sb.length() > 0) {
                sb.append(":");
            }

            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }
    
}
