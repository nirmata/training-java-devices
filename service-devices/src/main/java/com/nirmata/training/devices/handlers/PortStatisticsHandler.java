package com.nirmata.training.devices.handlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.Subscribe;
import com.nirmata.model.ModelController;
import com.nirmata.notify.ChangeNotification;
import com.nirmata.security.SecurityController;
import com.nirmata.training.devices.Devices;

public class PortStatisticsHandler {

    private final Logger _logger;
    private final SecurityController _sctrl;

    public PortStatisticsHandler(ModelController ctrl) {
        _sctrl = ctrl.getSecurityController();
        _logger = LoggerFactory.getLogger(getClass());
    }

    @Subscribe
    public void handlePortStatisticsChanges(ChangeNotification changes) {
        try {
            _sctrl.loginLocal();
            changes.getCreatedOrModifiedIds(Devices.PortStatistics).forEach(statsId->{
            	_logger.debug("push port stats to elasticsearch");
            	//pushPortStatisticsToElasticsearch(_ctrl,statsId);
            });

        } catch (Exception e) {
            _logger.error("Failed to execute createApplications: ", e);
        } finally {
            _sctrl.logout();
        }
    }
}
