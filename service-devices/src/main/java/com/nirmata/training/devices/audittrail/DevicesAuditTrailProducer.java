/**
 * Copyright Nirmata 2014
 */

package com.nirmata.training.devices.audittrail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.nirmata.audittrail.AuditTrailNotificationProducer;
import com.nirmata.audittrail.DefaultAuditTrailDescriptionBuilder;
import com.nirmata.model.BuiltIn;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelId;
import com.nirmata.model.alarms.Alarm;
import com.nirmata.notification.NotificationProducer;
import com.nirmata.training.devices.Device;
import com.nirmata.training.devices.Devices;

@Singleton
public class DevicesAuditTrailProducer extends AuditTrailNotificationProducer {

    private static final Logger _logger = LoggerFactory.getLogger(DevicesAuditTrailProducer.class);
    private final String TOPIC = "Analytics.Users";

    static {
        setIncludedModels();
        setSuppressedMods();
        setActions();
        setCreateFields();
        setStateChangeModels();
        setStateChangeFields();
        processStateChanges = true;
    }

    @Inject
    public DevicesAuditTrailProducer(ModelController ctrl, NotificationProducer producer) {
        super(ctrl, producer, new DefaultAuditTrailDescriptionBuilder(ctrl));
        _logger.debug("Create DevicesAuditTrailProducer");
    }

    private static void setIncludedModels() {
        INCLUDE_MODELS.addAll(ImmutableSet.of(
            Devices.Device,
            BuiltIn.Alarm,
            BuiltIn.AlarmNotifier));
    }

    private static void setSuppressedMods() {
        SUPPRESS_MODS.addAll(ImmutableSet.of(
            Device.sysUpTime,
            Alarm.stateChangedOn
            ));
    }

    private static void setActions() {
    }

    private static void setCreateFields() {

    }

    private static void setStateChangeModels() {
        STATECHANGE_MODELS.addAll(ImmutableSet.of(
        	Devices.Device));
    }

    private static void setStateChangeFields() {
        INCLUDE_STATECHANGE_FIELDS.putAll(
            ImmutableMap.of(
            	Devices.Device,
                ImmutableList.of(Device.status)));
    }

    @Override
    protected boolean isRoot(ModelId id) {
        return id.getModelIndex() == Devices.Root;
    }

    @Override
    protected String getTopic() {
        return TOPIC;
    }
}
