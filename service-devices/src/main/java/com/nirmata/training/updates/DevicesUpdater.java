package com.nirmata.training.updates;


import com.nirmata.model.update.AbstractModelUpdater;

public class DevicesUpdater extends AbstractModelUpdater {

    private static int NEXT_VERSION = 2;

    public DevicesUpdater() {

    }

    @Override
    protected String getUpdatePackage() {
        return "com.nirmata.training.updates";
    }

    @Override
    protected int getNextVersion() {
        return NEXT_VERSION;
    }

}