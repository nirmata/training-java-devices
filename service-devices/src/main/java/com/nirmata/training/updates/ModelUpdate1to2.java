package com.nirmata.training.updates;


import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.nirmata.model.ModelController;
import com.nirmata.model.ModelObject;
import com.nirmata.model.ModelTransaction;
import com.nirmata.model.ValidationException;
import com.nirmata.model.update.ModelUpdate;
import com.nirmata.query.ModelQuery;
import com.nirmata.store.ModelStore;
import com.nirmata.training.devices.Device;
import com.nirmata.training.devices.Devices;
import com.nirmata.training.devices.Port;
import com.nirmata.training.devices.PortStatistics;


public class ModelUpdate1to2 implements ModelUpdate {

    private final Logger _logger;

    public ModelUpdate1to2() {
        _logger = LoggerFactory.getLogger(getClass());
    }

    @Override
    public void update(ModelController ctrl, ModelStore store) {
        _logger.warn("Running model update {}", getClass().getName());

        // Use ModelTransaction to create PortStatistics object if it doesn't exists
        createPortStatistics(ctrl);
        
        // Use ModelStore to remove sysUpTimeAttribute from Device ModelObjects
        removeSysUpTimeAttribute(ctrl,store);
    }

    private void removeSysUpTimeAttribute(ModelController ctrl, ModelStore store) {
        ModelQuery query = ctrl.newQuery().modelClass(Devices.Device).build();
        store.transform(query, (Map<String, Object> dbo) -> {
            dbo.remove(Device.sysUpTime.getName());
            return dbo;
        });
    }
    

    private void createPortStatistics(ModelController ctrl) {
        ModelQuery query = ctrl.newQuery().modelClass(Devices.Port).build();
        
        ctrl.query(query).stream()
        	.filter(portId -> ctrl.getModelObject(portId).getRelation(Port.stats) == null)
        	.forEach(portId -> {
                try (ModelTransaction txn = ctrl.newTransaction()) {
                	txn.start();
                    ModelObject stats = txn.createObject(portId, Port.stats);
                    txn.modify(stats.getId(), PortStatistics.bytesIn, 0);
                    txn.modify(stats.getId(), PortStatistics.bytesOut, 0);
                    txn.commit();
                }	
                catch (ValidationException e) {
                	_logger.debug("Failed to create PortStatistics ",e);
                }
        	});
    }
}

