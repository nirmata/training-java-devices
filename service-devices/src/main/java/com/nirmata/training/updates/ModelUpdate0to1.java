package com.nirmata.training.updates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nirmata.model.*;
import com.nirmata.model.update.ModelUpdate;
import com.nirmata.query.ModelQuery;
import com.nirmata.store.ModelStore;
import com.nirmata.training.devices.*;

public class ModelUpdate0to1  implements ModelUpdate {

    private final Logger _logger;

    public ModelUpdate0to1() {
        _logger = LoggerFactory.getLogger(getClass());
    }

    @Override
    public void update(ModelController ctrl, ModelStore store) {
        _logger.warn("Running model update {}", getClass().getName());

        // Here we pretend that we introduce a new ModelClass in our Model: PortStatistics
        // The updater is going to create PortStatistics ModelObject for all the Ports
        // already created
        createPortStatistics(ctrl);

    }
   
    private void createPortStatistics(ModelController ctrl) {
        ModelQuery query = ctrl.newQuery().modelClass(Devices.Port).build();
        
        ctrl.query(query).stream()
        	.filter(portId -> ctrl.getModelObject(portId).getRelation(Port.stats) == null)
        	.forEach(portId -> {
                try (ModelTransaction txn = ctrl.newTransaction()) {
                	txn.start();
                    ModelObject stats = txn.createObject(portId, Port.stats);
                    txn.modify(stats.getId(), PortStatistics.bytesIn, 0);
                    txn.modify(stats.getId(), PortStatistics.bytesOut, 0);
                    txn.commit();
                }	
                catch (ValidationException e) {
                	_logger.debug("Failed to create PortStatistics ",e);
                }
        	});
    }
}

